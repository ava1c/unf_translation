  0_dict_uid.txt		Словарь метаданных УНФ - СМ составленный по УИД  
  1_dictionary_platform.xml	Словарь перевода платформы. Не самый свежий, но может быть полезен.   
  2. DictSB 1.5 23.03.2016.mxl	Словарь перевода УНФ на английский (SmallBusiness)  
  3. SSL slovar.txt		Словарь перевода английской БСП (та, которая используется, например, в ACS)  
  4. SSL (auto).xlsx		Cловарь перевода кода БСП 3  

```bash
grep -P '^.*?[а-яА-Я].*?\t' 'Dict.txt' > 'Dict filtered.txt' - оставляет строки с кириллицей
grep -P '\t.*?[a-zA-Z].*?\r' 'Dict filtered.txt' > 'DictSB filtered 2.txt' - оставляет с непустым переводом (после табуляции)
grep -v ' ' '2. DictSB filtered 2.txt' | sed 's/\.//g' > 'DictSB NoDotsAndSpaces.txt' - удаляет строки с пробелами, потом удаляет точки

dict_ru_en_metadata_uid_DictSB_ssl auto_ssl slovar.txt - словарь метаданных, составленный по уид, затем несопоставленное переведено по словарю DictSB, остаток по SSL (auto) и SSL slovar

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

sed -i 's/'='//' common_en.txt
grep -v '[\. ]' common_en.txt > common_en_clean.txt
grep -P '^.*?[а-яА-Я].*?$' 'common_en_clean.txt' > common_en_clean_ru.txt
join <(sort common_en_clean_ru.txt) <(sort -k1,1 0_dict_uid.txt) | sort > join_uid.txt
join -v 1 <(sort common_en_clean_ru.txt) <(sort -k1,1 0_dict_uid.txt) | sort > after_join_uid.txt
join <(sort -k1,1 after_join_uid.txt) <(sort -k1,1 2_DictSB_NoDotsAndSpaces.txt) | sort > join_uid_dictSB.txt
join -v 1 <(sort -k1,1 after_join_uid.txt) <(sort -k1,1 2_DictSB_NoDotsAndSpaces.txt) | sort > after_join_uid_dictSB.txt
join <(sort -k1,1 after_join_uid_dictSB.txt) <(sort -k1,1 4_SSL_auto.txt) | sort > join_uid_dictSB_ssl_auto.txt
join -v 1 <(sort -k1,1 after_join_uid_dictSB.txt) <(sort -k1,1 4_SSL_auto.txt) | sort > after_join_uid_dictSB_ssl_auto.txt
join <(sort -k1,1 after_join_uid_dictSB_ssl_auto.txt) <(sort -k1,1 3_SSL_sloar_NoDotsAndSpaces.txt) | sort > join_uid_dictSB_ssl_auto_ssl_slovar.txt
join -v 1 <(sort -k1,1 after_join_uid_dictSB_ssl_auto.txt) <(sort -k1,1 3_SSL_sloar_NoDotsAndSpaces.txt) | sort > after_join_uid_dictSB_ssl_auto_ssl_slovar.txt
cat join_uid.txt join_uid_dictSB.txt join_uid_dictSB_ssl_auto.txt join_uid_dictSB_ssl_auto_ssl_slovar.txt | awk '!seen[$0]++' > join_all.txt

awk 'n=x[$1]{print n"\n"$0;} {x[$1]=$0;}' join_all.txt # вывод дублей по колонке 1 ($2 - по второй, можно по сочетанию колонок)

# Чистка от дублей словаря (ключи)
awk 'n=x[$1]{print n"\n"$0;} {x[$1]=$0;}' join_all.txt > join_all_duplicate_keys.txt
join_all_duplicate_keys.txt  -->   join_all_duplicate_keys_removed.txt # вручную почистил разные переводы одного слова
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' join_all_duplicate_keys_removed.txt join_all.txt) join_all_duplicate_keys_removed.txt > join_all_uniq_keys.txt # сначала из файла join_all.txt удаляем все строки совпадающие по первой колонке с очищенным списком дублей join_all_duplicate_keys_removed.txt затем добавляем этот очищенный список в низ словаря, получаем словарь с уникальными ключами

# Чистка от дублей переводов (значения) ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' join_all_uniq_keys.txt join_all_uniq_keys.txt > join_all_uniq_keys_duplicate_values.txt # Словарь дублей переводов: пары "слово перевод"

Далее составил русский словарь УНФ с указанием объекта где использовано каждое слово, для этого в ЕДТ выгрузил английский язык в контекстный словарь, с настройкой "Заполнять отсутствующий перевод по исходному языку". Получили файлы .trans в каждой папке проекта.
Собираем всю информацию в один файл:
grep -Pr '--include=*.'trans '='  > 'D:\Users\admin1\Desktop\Company Management\Translation\all_trans_w_path.txt'

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

grep -Pv '\.Lines=|\.Content=|\.Description=|\.TaskDescription=|\.Comment=|^Comment=|\.Condition=' all_trans_w_path.txt | # Выбираем переводы только для нужных типов строк
# получили строки формата 'ПутьКФайлу:ПутьКПереводимойСтроке=ПереводимаяСтрока'
sed -E 's/:.*?=/=/' | # убираем ПутьКПереводимойСтроке (всё что между : и =)
grep -P '=[а-яА-Я]' | # оставляем строки с кириллицей
grep -v ' ' | # убираем строки с рпобелами
sort | uniq | sed 's/=/ /' > trans_w_path_filtered.txt

awk 'NR==FNR {file1[tolower($1)]=$0; next} tolower($2) in file1 {print $1 " " file1[tolower($2)]}' join_all_uniq_keys_duplicate_values.txt <(awk '!a[tolower($0)]++' trans_w_path_filtered.txt) > used_duplicates_grouped.txt # сворачиваем предыдущий список, оставляя только уникальные сочетания путь+слово, присоединяем переводы из файла join_all_uniq_keys_duplicate_values.txt 

awk 'n=x[tolower($1$3)]{print n"\n"$0;} {x[tolower($1$3)]=$0;}' used_duplicates_grouped.txt > used_duplicates_grouped_filtered.txt # оставляем строки, по которым есть одинаковые сочетания "путь - перевод" (для каждого пути остальись только уникальные ключи после предыдущей операции)
# получили список вида "путь слово перевод"

awk '!a[tolower($2$3)]++{print $2 " " $3}' used_duplicates_grouped_filtered.txt  | sort -k 2 > used_duplicates_grouped_filtered_uniq.txt # убрали путь, оставили уникальные сочетания "слово перевод"

used_duplicates_grouped_filtered_uniq.txt --> used_duplicates_grouped_filtered_uniq_removed.txt #  вручную изменил совпадающие переводы

cat <(awk 'FNR==NR {f2[tolower($1)];next} !(tolower($1) in f2)' used_duplicates_grouped_filtered_uniq_removed.txt join_all_uniq_keys2.txt) used_duplicates_grouped_filtered_uniq_removed.txt > join_all_uniq_keys_uniq_values.txt

# проверка что не осталось дублей переводов:
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' join_all_uniq_keys_uniq_values.txt join_all_uniq_keys_uniq_values.txt > join_all_uniq_keys_duplicate_values2.txt                                                          
awk 'NR==FNR {file1[$1]=$0; next} $2 in file1 {print $1 " " file1[$2]}' join_all_uniq_keys_duplicate_values2.txt <(awk '!a[$0]++' trans_w_path_filtered.txt) > used_duplicates_grouped2.txt
awk 'n=x[$1$3]{print n"\n"$0;} {x[$1$3]=$0;}' used_duplicates_grouped2.txt > used_duplicates_grouped_filtered2.txt                                                                            
awk '!a[$2$3]++{print $2 " " $3}' used_duplicates_grouped_filtered2.txt  | sort -k 2 > used_duplicates_grouped_filtered_uniq2.txt  

# Оставшиеся дубли перевода надо разделить хотя бы по регистру:
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' join_all_uniq_keys_uniq_values.txt join_all_uniq_keys_uniq_values.txt | sort -k2 > case_duplicates.txt

cat \
<(awk 'seen[$2]++ == 0' case_duplicates.txt) \
<(awk 'seen[$2]++ == 1' case_duplicates.txt \
| sed -r 's/( [A-Z]+)([A-Z][a-z].*$)/\L\1\E\2/' \
| sed -r 's/( [A-Z][a-z]+?)([A-Z].*)/\U\1\E\2/' \
| sed -r 's/( [A-Z][a-z])([a-z0-9]*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 2' case_duplicates.txt | sed -r 's/( [A-Z][a-z]+?)([A-Z][a-z]+)(.*)/\1\U\2\E\3/' | sed -r 's/( [A-Z][a-z]{2})([a-z]*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 3' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{3})(.*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 4' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{4})(.*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 5' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{5})(.*$)/\U\1\E\2/') | \
sort -k2 > case_duplicates_removed_auto.txt

cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' case_duplicates_removed_auto.txt join_all_uniq_keys_uniq_values.txt) case_duplicates_removed_auto.txt > join_all_uniq_keys_uniq_values_case_sensitive.txt

# вручную отредактировал остатки --> case_duplicates_removed_manual
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' case_duplicates_removed_manual.txt join_all_uniq_keys_uniq_values_case_sensitive.txt) case_duplicates_removed_manual.txt > join_all_uniq_keys_uniq_values_case_sensitive.txt

P.S. из конфигурации удалены наиболее объёмные по тексту объекты российской специфики - регламентированные отчёты, обработки ЭДО, классификаторы типа ВЕТИС и пр.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
find . -type f -name '*.trans' -delete

sed 's/ /=/' join_all_uniq_keys_uniq_values_case_sensitive.txt | sort | sed '1i#Translations for: common' > common_en.dict

# Недостающие переводы (с количеством вхождений):
awk 'FNR==NR {f1[$1];next} !($2 in f1) {print $2}' join_all_uniq_keys_uniq_values_case_sensitive_final.txt trans_w_path_filtered.txt | sort | uniq -c | sort -r > trans_w_path_filtered_missed.txt

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


УСТАРЕЛО:

# потом отфильтровал словарь - оставил строки с кириллицей, убрал строки с пробелами
grep -P '^.*?[а-яА-Я].*?' common_en2.txt > common_en2_filtered.txt
grep -v ' ' 'common_en2_filtered.txt' | sed 's/:=/ /' > common_en2_filtered_noSpaces.txt

# awk 'NR==FNR {file1[$1]=$0; next} {print $1 " " file1[$2]}' join_all_uniq_keys_duplicate_values.txt <(awk '!a[$0]++' common_en2_filtered_noSpaces.txt) > used_duplicates_grouped.txt # сворачиваем предыдущий список, оставляя только уникальные сочетания путь+слово, присоединяем переводы из файла join_all_uniq_keys_duplicate_values.txt 

# то же без учёта регистра (добавил tolower()):
awk 'NR==FNR {file1[tolower($1)]=$0; next} tolower($2) in file1 {print $1 " " file1[tolower($2)]}' join_all_uniq_keys_duplicate_values.txt <(awk '!a[tolower($0)]++' common_en2_filtered_noSpaces.txt) > used_duplicates_grouped.txt

в git bash команда grep некорректно работает с ключём -w: Например по запросу Отмена выдавались в том числе и строки ЧастичнаяОтгрузкаОтмена, ЗаказОтмена и т.д.
этот баг исправлен в новых версиях, я использовал последнюю версию GNU grep 3.4 из командной строки Ubuntu в Windows, работает быстро
в терминале ubuntu:
ava@DESKTOP-SUQB6IM:/mnt/d/EDT/UNF_16_no_comments/src$ grep -iro '--exclude=*.'{html,mdo,form,mxlx} -Ef '/mnt/d/Users/admin1/Desktop/Company Management/Translation/join_all_uniq_keys_duplicate_values_keys_w
.txt' | sed 's/:/ /' > '/mnt/d/Users/admin1/Desktop/Company Management/Translation/used_duplicates_word.txt'

ava@DESKTOP-SUQB6IM:/mnt/d/EDT/UNF_16_no_comments/src$ grep -iro '--include=*.'{mdo,form,mxlx} -Ff '/mnt/d/Users/admin1/Desktop/Company Management/Translation/join_all_uniq_keys_duplicate_values_keys_t.txt'
 | sed 's/:/ /' | sed 's/[<>]//g' > '/mnt/d/Users/admin1/Desktop/Company Management/Translation/used_duplicates_tag.txt'
 
отдельно парсим файлы mdo,form,mxlx - берём слово между скобочек >...<, остальное парсится как раньше \b...\b, исключаем из поиска файлы *.html

# перевод словаря строк по словарю файлов!
# afk работает очень медленно
awk -F'~' -vOFS='~' '
FNR==NR{
  a[$1]=$2;
  next
}
{
for(i in a) {
$2 = gensub("\\<([nt]?)"i"\\>", "\\1"a[i], "g", $2);
}
}
1
'  test_dict.txt  test.txt
```
