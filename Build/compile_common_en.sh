mkdir tmp

printf "фильтруем словарь 2_DictSB_1.5_23.03.2016.txt... \n\t"
grep -P '^.*?[а-яА-Я].*?\t' lib/2_DictSB_1.5_23.03.2016.txt | # оставляет строки с кириллицей
grep -P '\t.*?[a-zA-Z].*?\r' | # оставляет с непустым переводом (после табуляции)
grep -v '[ \.,/|]' | sed 's/[][)(?":}{%&!;*]//g' | awk '!seen[$0]++' > tmp/2_DictSB_NoDotsAndSpaces.txt # удаляет строки с пробелами, запятыми и точками, удаляет знаки препинания
echo 2_DictSB_NoDotsAndSpaces.txt

printf "фильтруем словарь 3_SSL_slovar.txt... \n\t"
grep -P '^.*?[а-яА-Я].*?\t' lib/3_SSL_slovar.txt | # оставляет строки с кириллицей
grep -P '\t.*?[a-zA-Z].*?\r' | # оставляет с непустым переводом (после табуляции)
grep -v '[ \.,/|]' | sed 's/[][)(?":}{%&!;*]//g' | awk '!seen[$0]++' > tmp/3_SSL_sloar_NoDotsAndSpaces.txt # удаляет строки с пробелами, запятыми и точками, удаляет знаки препинания
echo 3_SSL_sloar_NoDotsAndSpaces.txt

#Номенклатура: Items --> ProductsAndServices
cat <(awk -F"\t" 'FNR==NR {f2[$1];next} !($1 in f2)' assets/products_and_services_patch.txt tmp/3_SSL_sloar_NoDotsAndSpaces.txt)\
 assets/products_and_services_patch.txt > tmp/3_SSL_sloar_NoDotsAndSpaces_patched.txt

#Уникальные ключи во всех словарях - нужно для дальнейшего обучения нейросетки Custom translator
printf "Чистка словарей от дублей ключей... \n\t"
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/2_DictSB_NoDotsAndSpaces_duplicate_keys_removed.txt tmp/2_DictSB_NoDotsAndSpaces.txt)\
 assets/2_DictSB_NoDotsAndSpaces_duplicate_keys_removed.txt > tmp/2_DictSB_NoDotsAndSpaces_uniq_keys.txt
printf "2_DictSB_NoDotsAndSpaces_uniq_keys.txt \n\t"

cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/3_SSL_sloar_NoDotsAndSpaces_patched_duplicate_keys_removed.txt tmp/3_SSL_sloar_NoDotsAndSpaces_patched.txt)\
 assets/3_SSL_sloar_NoDotsAndSpaces_patched_duplicate_keys_removed.txt > tmp/3_SSL_sloar_NoDotsAndSpaces_uniq_keys.txt
printf "3_SSL_sloar_NoDotsAndSpaces_uniq_keys.txt \n\t"

cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/4_SSL_auto_duplicate_keys_removed.txt lib/4_SSL_auto.txt)\
 assets/4_SSL_auto_duplicate_keys_removed.txt > tmp/4_SSL_auto_uniq_keys.txt
printf "4_SSL_auto_uniq_keys.txt \n"
 
##############################################################################
 
printf "Подготовка пустого словаря common_en.txt... \n\t"
grep -v '[ \.,]' lib/unf_common_en_empty.dict | sed 's/=//' | sed 's/[][)(?":}{%&!;]//g' | awk '!seen[$0]++' | grep -P '^.*?[а-яА-Я].*?$' > tmp/common_en_ru_words.txt

echo common_en_ru_words.txt

printf "\nПоследовательный перевод по файлам словарей... \n\t"
join <(sort tmp/common_en_ru_words.txt) <(sort -k1b,1 lib/0_dict_uid.txt) > tmp/join_uid.txt
after_join_uid=$(join -v 1 <(sort tmp/common_en_ru_words.txt) <(sort -k1b,1 lib/0_dict_uid.txt))
join <(sort -k1b,1 <<<"$after_join_uid") <(sort -k1b,1 tmp/2_DictSB_NoDotsAndSpaces_uniq_keys.txt) > tmp/join_uid_dictSB.txt
after_join_uid_dictSB=$(join -v 1 <(sort -k1b,1 <<<"$after_join_uid") <(sort -k1b,1 tmp/2_DictSB_NoDotsAndSpaces_uniq_keys.txt)) 
unset after_join_uid
join <(sort -k1b,1 <<<"$after_join_uid_dictSB") <(sort -k1b,1 tmp/4_SSL_auto_uniq_keys.txt) > tmp/join_uid_dictSB_ssl_auto.txt
after_join_uid_dictSB_ssl_auto=$(join -v 1 <(sort -k1b,1 <<<"$after_join_uid_dictSB") <(sort -k1b,1 tmp/4_SSL_auto_uniq_keys.txt))
unset after_join_uid_dictSB
join <(sort -k1b,1 <<<"$after_join_uid_dictSB_ssl_auto") <(sort -k1b,1 tmp/3_SSL_sloar_NoDotsAndSpaces_uniq_keys.txt) > tmp/join_uid_dictSB_ssl_auto_ssl_slovar.txt
unset after_join_uid_dictSB_ssl_auto
cat tmp/join_uid.txt tmp/join_uid_dictSB.txt tmp/join_uid_dictSB_ssl_auto.txt tmp/join_uid_dictSB_ssl_auto_ssl_slovar.txt | \
sed 's/[][)(?":}{%&!;]//g' | awk '!seen[$0]++' | sort > tmp/join_all.txt # убираем знаки препинания, оставляем только уникальные значения
join -v 1 <(sort tmp/common_en_ru_words.txt) <(sort -k1b,1 tmp/join_all.txt) | sort > not_translated_words.txt # то что не перевелось
echo join_all.txt
echo Слова оставшиеся без перевода: not_translated_words.txt


printf "чистка от дублей ключей... \n\t"
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/join_all_duplicate_keys_removed.txt tmp/join_all.txt) assets/join_all_duplicate_keys_removed.txt\
 > tmp/join_all_uniq_keys.txt
echo join_all_uniq_keys.txt 

printf "чистка от дублей переводов... \n"

#  ...вручную изменил совпадающие переводы -> used_duplicates_grouped_filtered_uniq_removed.txt
dict_edited=$(cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/used_duplicates_grouped_filtered_uniq_removed.txt tmp/join_all_uniq_keys.txt) assets/used_duplicates_grouped_filtered_uniq_removed.txt)

printf "добавляем словарь спецификаций... \n"
dict_spec=$(cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' <(sed 's/=/ /' lib/specifications.dict)  <(printf '%s\n' "$dict_edited"))\
 <(sed 's/=/ /' lib/specifications.dict))
dict_edited=''

printf "добавляем словарь языка запросов...\n\t"
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' lib/5_QUERY.txt  <(printf '%s\n' "$dict_spec")) lib/5_QUERY.txt > tmp/join_all_uniq_keys_uniq_values.txt
dict_spec=''
printf "join_all_uniq_keys_uniq_values.txt\n"

printf "оставшиеся дубли переводов:\n\t"
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' tmp/join_all_uniq_keys_uniq_values.txt tmp/join_all_uniq_keys_uniq_values.txt | sort -k2\
 > tmp/join_all_uniq_keys_duplicate_values.txt
echo join_all_uniq_keys_duplicate_values.txt

# 1 Выбираем переводы только для нужных типов строк
# получили строки формата 'ПутьКФайлу:ПутьКПереводимойСтроке=ПереводимаяСтрока'
# 2 убираем ПутьКПереводимойСтроке (всё что между : и =)
# 3 убираем строки с рпобелами
# 4 к полученному списку добавляем перечень имён объектов метаданных из файла Configuration.mdo
# 5 оставляем строки с кириллицей
# 6 сортируем и оставляем только уникальные строки
printf "\nОтбор переводимых строк... \n\t"
cat \
<(grep -Pv '\.Lines=|\.Content=|\.Description=|\.TaskDescription=|\.Comment=|^Comment=|\.Condition=' lib/all_trans_w_path.txt |\
   sed -E 's/:.*?=/=/' |\
   grep -v ' ') \
<(grep -Po '\..*?\<\/' lib/Configuration.mdo | \
   sed 's/[\<\/]//g' | 
   sed 's/\./Configuration\.mdo=/') |\
 grep -P '=.*?[а-яА-Я]+' |\
 sort | uniq | sed 's/=/ /' \
 > tmp/trans_w_path_ru_filtered.txt
 
echo trans_w_path_ru_filtered.txt
 
cd tmp
 
printf "\nПоиск опасных дублей... \n\t"
# сворачиваем предыдущий список, оставляя только уникальные сочетания путь+слово, присоединяем переводы из файла join_all_uniq_keys_duplicate_values.txt: 
awk 'NR==FNR {file1[$1]=$0; next} $2 in file1 {print $1 " " file1[$2]}' join_all_uniq_keys_duplicate_values.txt <(awk '!a[tolower($0)]++' trans_w_path_ru_filtered.txt)\
 > used_duplicates_grouped.txt
# оставляем строки, по которым есть одинаковые сочетания "путь - перевод" (для каждого пути остальись только уникальные ключи после предыдущей операции):
awk 'n=x[$1$3]{print n"\n"$0;} {x[$1$3]=$0;}' used_duplicates_grouped.txt > used_duplicates_grouped_filtered.txt 
# получили список вида "путь слово перевод"
# убрали путь, оставили уникальные сочетания "слово перевод":
awk '!a[tolower($2$3)]++{print $2 " " $3}' used_duplicates_grouped_filtered.txt  | sort -k 2 > used_duplicates_grouped_filtered_uniq.txt 
echo used_duplicates_grouped_filtered_uniq.txt

printf "Оставшиеся дубли перевода надо разделить хотя бы по регистру... \n\t"
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' join_all_uniq_keys_uniq_values.txt join_all_uniq_keys_uniq_values.txt | sort -k2 > case_duplicates.txt
cat \
<(awk 'seen[$2]++ == 0' case_duplicates.txt) \
<(awk 'seen[$2]++ == 1' case_duplicates.txt |\
 sed -r 's/( [A-Z]+)([A-Z][a-z].*$)/\L\1\E\2/' |\
 sed -r 's/( [A-Z][a-z]+?)([A-Z].*)/\U\1\E\2/' |\
 sed -r 's/( [A-Z][a-z])([a-z0-9]*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 2' case_duplicates.txt | sed -r 's/( [A-Z][a-z]+?)([A-Z][a-z]+)(.*)/\1\U\2\E\3/' | sed -r 's/( [A-Z][a-z]{2})([a-z]*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 3' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{3})(.*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 4' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{4})(.*$)/\U\1\E\2/') \
<(awk 'seen[$2]++ == 5' case_duplicates.txt | sed -r 's/( [A-Z][a-z]{5})(.*$)/\U\1\E\2/') |\
 sort -k2 > case_duplicates_removed_auto.txt
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' case_duplicates_removed_auto.txt join_all_uniq_keys_uniq_values.txt) case_duplicates_removed_auto.txt\
 > join_all_uniq_keys_uniq_values_case_sensitive.txt
echo join_all_uniq_keys_uniq_values_case_sensitive.txt
 
cd ..
 
# вручную отредактировал остатки --> case_duplicates_removed_manual
printf "Вносим ручые правки...\n\t"
cat <(awk 'FNR==NR {f2[$1];next} !($1 in f2)' assets/case_duplicates_removed_manual.txt tmp/join_all_uniq_keys_uniq_values_case_sensitive.txt)\
 assets/case_duplicates_removed_manual.txt > tmp/join_all_uniq_keys_uniq_values_case_sensitive_final.txt
echo join_all_uniq_keys_uniq_values_case_sensitive_final.txt

printf "Оставшиеся дубли перевода: \n\t"
awk 'NR==FNR{s[$2]++;next} (s[$2]>1)' tmp/join_all_uniq_keys_uniq_values_case_sensitive_final.txt tmp/join_all_uniq_keys_uniq_values_case_sensitive_final.txt | sort -k2 > tmp/case_duplicates_final.txt
echo case_duplicates_final.txt

grep -P '\.Lines=' lib/all_trans_w_path.txt |\
 sed -r 's/^.*?\.String\.(.*?)\.Lines=/\1~/' |\
# в контекстном словаре ключ сохраняется с символами пробела, табуляции и переноса строки в начале и конце строки, а в общем словаре они обрезаются; удаляем эти символы чтобы преобразовать формат от контекстного словаря к общему:
 sed -r 's/^(\\\ |\\t|\\n)*// ; s/(\\\ |\\t|\\n)*~/~/' |\
 awk -F'~' '!seen[$2]++' > tmp/trans_w_path_lines.txt
printf "\nсловарь строк trans_w_path_lines.txt"

printf "\nперевод словаря строк по словарю слов... \n\t"
cd tmp
sed -r 's/(^.*?) (.*)/s\/\\b(\[nt\]\?)\1\\b\/\\1\2\/g/' join_all_uniq_keys_uniq_values_case_sensitive_final.txt > common_en_script.txt
time sed -rf common_en_script.txt <(cut -d~ -f2 trans_w_path_lines.txt) > trans_w_path_lines_values_trans.txt
paste -d' ' <(cut -d~ -f1 trans_w_path_lines.txt) trans_w_path_lines_values_trans.txt > trans_w_path_lines_translated.txt
echo trans_w_path_lines_translated.txt

cd ..

printf "\nитоговый файл словаря: \n\t"
cat tmp/join_all_uniq_keys_uniq_values_case_sensitive_final.txt tmp/trans_w_path_lines_translated.txt |\
 LC_COLLATE=C.UTF-8 sort -k1b,1 |\
 sed -r 's/([^\\]) /\1=/' | sed '1i#Translations for: common' |\
 perl -p -e 's/\n/\r\n/'\
 > common_en.dict
echo common_en.dict
echo
read -n 1 -s -r -p "Press any key to exit"