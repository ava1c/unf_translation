До перевода:
1. Поправил пустые имена command bar в формах
2. Общ. модуль ЦифроваяПодписьСлужебный Функция Sign --> SignObject
3. закомментировать проверку на язык скрипта
4. global replace СтруктурнаяЕдинца --> СтруктурнаяЕдиница
После перевода:
1. заменить XDTO пакеты
2. global replace 
VALUE(Enumeration. --> VALUE(Enum.
3. заменить описание типов
	TypeDescription("Row" - TypeDescription("String"
	TypeDescription("NUm" - TypeDescription("Number"
4. убрать лишний символ # в запросах: ВЫБРАТЬ ... ИЗ ##ИмяТаблицы

x. Common module InfobaseUpdateService: 
For Each SubsystemName In SubsystemDescriptions.Order Do --> For Each SubsystemName In SubsystemDescriptions.SortOrder Do
ПроверяемыйОбработчик.ID --> ПроверяемыйОбработчик.Identifier
НайденныйОбработчик.ID --> НайденныйОбработчик.Identifier
СтарыйОбработчик.ID --> СтарыйОбработчик.Identifier
6. Catalog MetadataObjectIDs ManagerModule:
	Unloading.Sort("ЭтоКоллекция УБЫВ,
	                     |ПометкаУдаления ВОЗР,
	                     |БезКлючаОбъектаМетаданных ВОЗР");
	Unloading.Sort("IsCollection DESC,
	                     |DeletionMark ASC,
	                     |WithoutMetadataObjectKey ASC");
						 
7. Catalog MetadataObjectIDs ManagerModule:	
	RenamingTable.Sort(
		"ПорядокБиблиотеки ВОЗР,
		|ВерсияЧасть1 ВОЗР,
		|ВерсияЧасть2 ВОЗР,
		|ВерсияЧасть3 ВОЗР,
		|ВерсияЧасть4 ВОЗР,
		|ПорядокДобавления ВОЗР");
	RenamingTable.Sort(
		"LibraryOrder ASC,
		|VersionPart1 ASC,
		|VersionPart2 ASC,
		|VersionPart3 ASC,
		|VersionPart4 ASC,
		|AddingOrder ASC");		
8. Common module AccessManagementService Function TextOfQuerySelectionChanges
// Создает запрос выбора различий между строками регистра в заданной области данных
// (на основе отборов в параметре ПоляИОтбор).
//
// Parameters:
//  SelectNewQueryText - String
//
//  FieldAndSelecting   - Массив элементов типа Структура("ИмяПоля", ИмяПараметраУсловияОтбора).
//
//  FullNameOfRegister
//               - Строка       - запрос старых формируется автоматически.
//                 Неопределено - запрос старых берется из следующего параметра.
//
//  TextOfQuerySelectOld
//               - Строка       - запрос старых, с учетом нестандартных отборов.
//               - Неопределено - когда полное имя регистра определено.
//
// Returns:
//  String - текст запрос с учетом оптимизации для СУБД PostgreSQL.
//
Function TextOfQuerySelectionChanges(SelectNewQueryText,
                                    FieldAndSelecting,
                                    FullNameOfRegister            = Undefined,
                                    TemporaryTablesQueryText = Undefined,
                                    TextOfQuerySelectOld     = Undefined) Export
	
	// Подготовка текста запроса старых данных.
	If FullNameOfRegister <> Undefined Then
		TextOfQuerySelectOld =
		"SELECT
		|	&SelectedField,
		|	&InsertFieldsRowChangeKind
		|FROM
		|	FullNameOfRegister AS OldData
		|WHERE
		|	&FilterConditions";
	EndIf;
	
	SelectedField = "";
	FilterConditions = "True";
	For Each FieldDetails In FieldAndSelecting Do
		// Сборка выбираемых полей.
		SelectedField = SelectedField + StrReplace(
			"
			|	OldData.Field,",
			"Field",
			KeyAndValue(FieldDetails).Key);
			
		// Сборка условий отбора.
		If ValueIsFilled(KeyAndValue(FieldDetails).Value) Then
			FilterConditions = FilterConditions + StrReplace(
				"
				|	AND &ParameterNameFilterConditions", "&ParameterNameFilterConditions",
				KeyAndValue(FieldDetails).Value);
		EndIf;
	EndDo;
	
	TextOfQuerySelectOld =
		StrReplace(TextOfQuerySelectOld, "&SelectedField,",  SelectedField);
	
	TextOfQuerySelectOld =
		StrReplace(TextOfQuerySelectOld, "&FilterConditions",    FilterConditions);
	
	TextOfQuerySelectOld =
		StrReplace(TextOfQuerySelectOld, "FullNameOfRegister", FullNameOfRegister);
	
	If StrFind(SelectNewQueryText, "&InsertFieldsRowChangeKind") = 0 Then
		ErrorText = StringFunctionsClientServer.PlaceParametersIntoString(
			NStr("ru = 'Ошибка в значении параметра ТекстЗапросаВыбораСтарых
			           |процедуры ТекстЗапросаВыбораИзменений модуля УправлениеДоступомСлужебный.
			           |
			           |В тексте запроса не найдена строка ""%1"".';
						|en = 'Ошибка в значении параметра ТекстЗапросаВыбораСтарых
						|процедуры ТекстЗапросаВыбораИзменений модуля УправлениеДоступомСлужебный.
						|
						|В тексте запроса не найдена строка ""%1"".';"),
			"&InsertFieldsRowChangeKind");
		Raise ErrorText;
	EndIf;
	
	TextOfQuerySelectOld = StrReplace(
		TextOfQuerySelectOld, "&InsertFieldsRowChangeKind", "-1 AS RowChangeKind");
	
	If StrFind(SelectNewQueryText, "&InsertFieldsRowChangeKind") = 0 Then
		ErrorText = StringFunctionsClientServer.PlaceParametersIntoString(
			NStr("ru = 'Ошибка в значении параметра ТекстЗапросаВыбораНовых
			           |процедуры ТекстЗапросаВыбораИзменений модуля УправлениеДоступомСлужебный.
			           |
			           |В тексте запроса не найдена строка ""%1"".';
						|en = 'Ошибка в значении параметра ТекстЗапросаВыбораНовых
						|процедуры ТекстЗапросаВыбораИзменений модуля УправлениеДоступомСлужебный.
						|
						|В тексте запроса не найдена строка ""%1"".';"),
			"&InsertFieldsRowChangeKind");
		Raise ErrorText;
	EndIf;
	
	SelectNewQueryText = StrReplace(
		SelectNewQueryText,  "&InsertFieldsRowChangeKind", "1 AS RowChangeKind");
	
	// Подготовка текста запроса выбора изменений.
	QueryText =
	"SELECT
	|	&SelectedField,
	|	SUM(AllRows.RowChangeKind) AS RowChangeKind
	|FROM
	|	(SelectNewQueryText
	|	
	|	UNION ALL
	|	
	|	TextOfQuerySelectOld) AS AllRows
	|	
	|GROUP BY
	|	&GroupFields
	|	
	|HAVING
	|	SUM(AllRows.RowChangeKind) <> 0";
	
	SelectedField = "";
	GroupFields = "";
	For Each FieldDetails In FieldAndSelecting Do
		// Сборка выбираемых полей.
		SelectedField = SelectedField + StrReplace(
			"
			|	AllRows.Field,",
			"Field",
			KeyAndValue(FieldDetails).Key);
		
		// Сборка полей соединения.
		GroupFields = GroupFields + StrReplace(
			"
			|	AllRows.Field,",
			"Field",
			KeyAndValue(FieldDetails).Key);
	EndDo;
	GroupFields = Left(GroupFields, StrLen(GroupFields)-1);
	QueryText = StrReplace(QueryText, "&SelectedField,",  SelectedField);
	QueryText = StrReplace(QueryText, "&GroupFields", GroupFields);
	
	QueryText = StrReplace(
		QueryText, "SelectNewQueryText",  SelectNewQueryText);
	
	QueryText = StrReplace(
		QueryText, "TextOfQuerySelectOld", TextOfQuerySelectOld);
	
	If ValueIsFilled(TemporaryTablesQueryText) Then
		QueryText = TemporaryTablesQueryText
		+ "
		  |;
		  |" + QueryText;
	EndIf;
	
	Return QueryText;
	
EndFunction
x. Common module AccessManagementService Function WriteMultipleSets
Function WriteMultipleSets(Data, Filter, FieldName, ValuesFields)
	
	Query = New Query;
	Query.SetParameter("ValuesFields", ValuesFields);
	Query.Text =
	"SELECT
	|	COUNT(*) AS Count
	|FROM
	|	&CurrentTable AS CurrentTable
	|WHERE
	|	&FilterCondition
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	COUNT(*) AS Count
	|FROM
	|	&CurrentTable AS CurrentTable
	|WHERE
	|	CurrentTable.FieldName IN(&ValuesFields)
	|	AND &FilterCondition
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	CurrentTable.FieldName AS FieldName,
	|	COUNT(*) AS Count
	|FROM
	|	&CurrentTable AS CurrentTable
	|WHERE
	|	CurrentTable.FieldName IN(&ValuesFields)
	|	AND &FilterCondition
	|
	|GROUP BY
	|	CurrentTable.FieldName";
	
	FilterCondition = "True";
	If Data.FixedSelection <> Undefined Then
		For Each KeyAndValue In Data.FixedSelection Do
			FilterCondition = FilterCondition + "
			|	AND CurrentTable." + KeyAndValue.Key + " = &" + KeyAndValue.Key;
			Query.SetParameter(KeyAndValue.Key, KeyAndValue.Value);
		EndDo;
	EndIf;
	
	FilterOfAdding = New Structure;
	FilterOfAdding.Insert("RowChangeKind", 1);
	DeletedFilter = New Structure;
	DeletedFilter.Insert("RowChangeKind", -1);
	
	For Each KeyAndValue In Filter Do
		FilterCondition = FilterCondition + "
		|	AND CurrentTable." + KeyAndValue.Key + " = &" + KeyAndValue.Key;
		Query.SetParameter(KeyAndValue.Key, KeyAndValue.Value);
		FilterOfAdding.Insert(KeyAndValue.Key, KeyAndValue.Value);
		DeletedFilter.Insert(KeyAndValue.Key, KeyAndValue.Value);
	EndDo;
	
	Query.Text = StrReplace(Query.Text, "FieldName", FieldName);
	Query.Text = StrReplace(Query.Text, "&CurrentTable", Data.FullNameOfRegister);
	Query.Text = StrReplace(Query.Text, "&FilterCondition", FilterCondition);
	
	QueryResults = Query.ExecuteBatch();
	
	// Количество всех без отбора.
	CountOfAll = QueryResults[0].Unload()[0].Count;
	Data.Insert("NumberOfRead", CountOfAll);
	
	// Количество обновляемых с отбором.
	CountOfUpdated = QueryResults[1].Unload()[0].Count;
	
	AddedCount = Data.NewRecords.FindRows(FilterOfAdding).Count();
	If AddedCount > CountOfUpdated Then
		CountOfUpdated = AddedCount;
	EndIf;
	
	ToDeleteCount = Data.NewRecords.FindRows(DeletedFilter).Count();
	If ToDeleteCount > CountOfUpdated Then
		CountOfUpdated = ToDeleteCount;
	EndIf;
	
	// Количество для чтения по значениям отбора.
	NumberOfValues = QueryResults[2].Unload();
	NumberOfValues.Indexes.Add(FieldName);
	Data.Insert("NumberOfValues", NumberOfValues);
	
	Return CountOfAll * 0.7 > CountOfUpdated;
	
EndFunction
x. Common module AccessManagementService Procedure ReadCountForReading
Procedure ReadCountForReading(Data)
	
	Query = New Query;
	Query.Text =
	"SELECT
	|	COUNT(*) AS Count
	|FROM
	|	&CurrentTable AS CurrentTable
	|WHERE
	|	&FilterCondition";
	
	FilterCondition = "True";
	If Data.FixedSelection <> Undefined Then
		For Each KeyAndValue In Data.FixedSelection Do
			FilterCondition = FilterCondition + "
			|	AND CurrentTable." + KeyAndValue.Key + " = &" + KeyAndValue.Key;
			Query.SetParameter(KeyAndValue.Key, KeyAndValue.Value);
		EndDo;
	EndIf;
	
	Query.Text = StrReplace(Query.Text, "&CurrentTable", Data.FullNameOfRegister);
	Query.Text = StrReplace(Query.Text, "&FilterCondition", FilterCondition);
	
	Data.Insert("NumberOfRead", Query.Execute().Unload()[0].Count);
	
EndProcedure
x. Common module DataExchangeServer
ProfileDescription.ID --> ProfileDescription.Identifier
x. Catalog AccessGroupsProfiles
ProfileDescription.ID --> ProfileDescription.Identifier
x. Information registers AccessGroupsTables ManagerModule
// Процедура обновляет данные регистра при изменении состава ролей профилей и
// изменении профилей у групп доступа.
//
// Parameters:
//  AccessGroups - CatalogRef.AccessGroups
//                - Массив значений указанного выше типа.
//                - Undefined - без отбора.
//
//  Tables       - CatalogRef.MetadataObjectIDs
//                - Массив значений указанного выше типа.
//                - Undefined - без отбора.
//
//  HasChanges - Boolean - (возвращаемое значение) - если производилась запись,
//                  устанавливается Истина, иначе не изменяется.
//
Procedure RefreshDataRegister(AccessGroups = Undefined,
                                 Tables       = Undefined,
                                 HasChanges = Undefined) Export
	
	If TypeOf(Tables) = Type("Array") Or TypeOf(Tables) = Type("FixedArray") Then
		RecCount = Tables.Count();
		If RecCount = 0 Then
			Return;
		ElsIf RecCount > 500 Then
			Tables = Undefined;
		EndIf;
	EndIf;
	
	If Catalogs.ExtensionVersions.ExtensionsChangedDynamically() Then
		Raise NStr("ru = 'Расширения конфигурации обновлены, требуется перезапустить сеанс.';
								|en = 'Расширения конфигурации обновлены, требуется перезапустить сеанс.';");
	EndIf;
	
	InformationRegisters.RolesRights.ПроверитьДанныеРегистра();
	
	SetPrivilegedMode(True);
	
	QueryBlankRecords = New Query;
	QueryBlankRecords.Text =
	"SELECT TOP 1
	|	TRUE AS TrueValue
	|FROM
	|	InformationRegister.AccessGroupsTables AS AccessGroupsTables
	|WHERE
	|	AccessGroupsTables.Table = VALUE(Catalog.MetadataObjectIDs.EmptyRef)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT TOP 1
	|	TRUE AS TrueValue
	|FROM
	|	InformationRegister.AccessGroupsTables AS AccessGroupsTables
	|WHERE
	|	AccessGroupsTables.AccessGroup = VALUE(Catalog.AccessGroups.EmptyRef)";
	
	TemporaryTablesQueryText =
	"SELECT
	|	ПраваРолейРасширений.MetadataObject AS MetadataObject,
	|	ПраваРолейРасширений.Role AS Role,
	|	ПраваРолейРасширений.Update AS Update,
	|	ПраваРолейРасширений.INsert AS INsert,
	|	ПраваРолейРасширений.ReadingNotLimited AS ReadingNotLimited,
	|	ПраваРолейРасширений.ChangingNotLimited AS ChangingNotLimited,
	|	ПраваРолейРасширений.AddingNotLimited AS AddingNotLimited,
	|	ПраваРолейРасширений.RowChangeKind AS RowChangeKind
	|INTO ПраваРолейРасширений
	|FROM
	|	&ПраваРолейРасширений AS ПраваРолейРасширений
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ПраваРолейРасширений.MetadataObject AS MetadataObject,
	|	ПраваРолейРасширений.Role AS Role,
	|	ПраваРолейРасширений.Update AS Update,
	|	ПраваРолейРасширений.INsert AS INsert,
	|	ПраваРолейРасширений.ReadingNotLimited AS ReadingNotLimited,
	|	ПраваРолейРасширений.ChangingNotLimited AS ChangingNotLimited,
	|	ПраваРолейРасширений.AddingNotLimited AS AddingNotLimited
	|INTO RolesRights
	|FROM
	|	ПраваРолейРасширений AS ПраваРолейРасширений
	|WHERE
	|	ПраваРолейРасширений.RowChangeKind = 1
	|
	|UNION ALL
	|
	|SELECT
	|	RolesRights.MetadataObject,
	|	RolesRights.Role,
	|	RolesRights.Update,
	|	RolesRights.INsert,
	|	RolesRights.ReadingNotLimited,
	|	RolesRights.ChangingNotLimited,
	|	RolesRights.AddingNotLimited
	|FROM
	|	InformationRegister.RolesRights AS RolesRights
	|		LEFT JOIN ПраваРолейРасширений AS ПраваРолейРасширений
	|		ON RolesRights.MetadataObject = ПраваРолейРасширений.MetadataObject
	|			AND RolesRights.Role = ПраваРолейРасширений.Role
	|WHERE
	|	ПраваРолейРасширений.MetadataObject IS NULL
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	AccessGroups.Ref AS Ref,
	|	AccessGroups.Profile AS Profile
	|INTO AccessGroups
	|FROM
	|	Catalog.AccessGroups AS AccessGroups
	|		INNER JOIN Catalog.AccessGroupsProfiles AS AccessGroupsProfiles
	|		ON AccessGroups.Profile = AccessGroupsProfiles.Ref
	|			AND (AccessGroups.Profile <> VALUE(Catalog.AccessGroupsProfiles.IRegUserInfo))
	|			AND (NOT AccessGroups.DeletionMark)
	|			AND (NOT AccessGroupsProfiles.DeletionMark)
	|			AND (&AccessGroupFilterCondition1)
	|			AND (TRUE IN
	|				(SELECT TOP 1
	|					TRUE AS TrueValue
	|				FROM
	|					Catalog.AccessGroups.Users AS УчастникиГруппДоступа
	|				WHERE
	|					УчастникиГруппДоступа.Ref = AccessGroups.Ref))
	|
	|INDEX BY
	|	AccessGroups.Profile
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT DISTINCT
	|	AccessGroups.Profile AS Ref
	|INTO Profiles
	|FROM
	|	AccessGroups AS AccessGroups
	|
	|INDEX BY
	|	AccessGroups.Profile
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT
	|	ProfilesRoles.Ref AS Profile,
	|	RolesRights.MetadataObject AS Table,
	|	RolesRights.MetadataObject.EmptyRefValue AS TableType,
	|	MAX(RolesRights.Update) AS Update,
	|	MAX(RolesRights.Update)
	|		AND MAX(RolesRights.INsert) AS INsert,
	|	MAX(RolesRights.ReadingNotLimited) AS ReadingNotLimited,
	|	MAX(RolesRights.ChangingNotLimited) AS ChangingNotLimited,
	|	MAX(RolesRights.ChangingNotLimited)
	|		AND MAX(RolesRights.AddingNotLimited) AS AddingNotLimited
	|INTO ProfilesTables
	|FROM
	|	Catalog.AccessGroupsProfiles.Roles AS ProfilesRoles
	|		INNER JOIN Profiles AS Profiles
	|		ON (Profiles.Ref = ProfilesRoles.Ref)
	|		INNER JOIN RolesRights AS RolesRights
	|		ON (&TableFilterCondition1)
	|			AND (RolesRights.Role = ProfilesRoles.Role)
	|			AND (NOT RolesRights.Role.DeletionMark)
	|			AND (NOT RolesRights.MetadataObject.DeletionMark)
	|
	|GROUP BY
	|	ProfilesRoles.Ref,
	|	RolesRights.MetadataObject,
	|	RolesRights.MetadataObject.EmptyRefValue
	|
	|INDEX BY
	|	RolesRights.MetadataObject
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|SELECT DISTINCT
	|	ProfilesTables.Table AS Table,
	|	AccessGroups.Ref AS AccessGroup,
	|	ProfilesTables.Update AS Update,
	|	ProfilesTables.INsert AS INsert,
	|	ProfilesTables.ReadingNotLimited AS ReadingNotLimited,
	|	ProfilesTables.ChangingNotLimited AS ChangingNotLimited,
	|	ProfilesTables.AddingNotLimited AS AddingNotLimited,
	|	ProfilesTables.TableType AS TableType
	|INTO NewData
	|FROM
	|	ProfilesTables AS ProfilesTables
	|		INNER JOIN AccessGroups AS AccessGroups
	|		ON (AccessGroups.Profile = ProfilesTables.Profile)
	|
	|INDEX BY
	|	ProfilesTables.Table,
	|	AccessGroups.Ref";
	
	QueryText =
	"SELECT
	|	NewData.Table,
	|	NewData.AccessGroup,
	|	NewData.Update,
	|	NewData.INsert,
	|	NewData.ReadingNotLimited,
	|	NewData.ChangingNotLimited,
	|	NewData.AddingNotLimited,
	|	NewData.TableType,
	|	&InsertFieldsRowChangeKind
	|FROM
	|	NewData AS NewData";
	
	// Подготовка выбираемых полей с необязательным отбором.
	Fields = New Array; 
	Fields.Add(New Structure("Table",       "&TableFilterCondition2"));
	Fields.Add(New Structure("AccessGroup", "&AccessGroupFilterCondition2"));
	Fields.Add(New Structure("Update"));
	Fields.Add(New Structure("INsert"));
	Fields.Add(New Structure("ReadingNotLimited"));
	Fields.Add(New Structure("ChangingNotLimited"));
	Fields.Add(New Structure("AddingNotLimited"));
	Fields.Add(New Structure("TableType"));
	
	Query = New Query;
	Query.SetParameter("ПраваРолейРасширений", AccessManagementService.ПраваРолейРасширений());
	Query.Text = AccessManagementService.TextOfQuerySelectionChanges(
		QueryText, Fields, "InformationRegister.AccessGroupsTables", TemporaryTablesQueryText);
	
	AccessManagementService.SetCriteriaForQuery(Query, Tables, "Tables",
		"&TableFilterCondition1:RolesRights.METADATAObject
		|&TableFilterCondition2:OldData.Table");
	
	AccessManagementService.SetCriteriaForQuery(Query, AccessGroups, "AccessGroups",
		"&AccessGroupFilterCondition1:AccessGroups.Ref
		|&AccessGroupFilterCondition2:OldData.AccessGroup");
		
	BLOck = New DataLock;
	BLOck.Add("InformationRegister.AccessGroupsTables");
	
	BeginTransaction();
	Try
		BLOck.Lock();
		Results = QueryBlankRecords.ExecuteBatch();
		If Not Results[0].IsEmpty() Then
			StandardSubsystemsServer.ПроверитьДинамическоеОбновлениеВерсииПрограммы();
			RecordSet = CreateRecordSet();
			RecordSet.Filter.Table.Set(Catalogs.MetadataObjectIDs.EmptyRef());
			RecordSet.Write();
			HasChanges = True;
		EndIf;
		If Not Results[1].IsEmpty() Then
			StandardSubsystemsServer.ПроверитьДинамическоеОбновлениеВерсииПрограммы();
			RecordSet = CreateRecordSet();
			RecordSet.Filter.AccessGroup.Set(Catalogs.AccessGroups.EmptyRef());
			RecordSet.Write();
			HasChanges = True;
		EndIf;
		
		If AccessGroups <> Undefined
		   And Tables        = Undefined Then
			
			DimensionsOfSelection = "AccessGroup";
		Else
			DimensionsOfSelection = Undefined;
		EndIf;
		
		Data = New Structure;
		Data.Insert("ManagerRegister",      InformationRegisters.AccessGroupsTables);
		Data.Insert("ChangeRowsContent", Query.Execute().Unload());
		Data.Insert("DimensionsOfSelection",       DimensionsOfSelection);
		
		IsCurrentChanges = False;
		AccessManagementService.RefreshInformationRegister(Data, IsCurrentChanges);
		If IsCurrentChanges Then
			StandardSubsystemsServer.ПроверитьДинамическоеОбновлениеВерсииПрограммы();
			HasChanges = True;
		EndIf;
		
		If IsCurrentChanges
		   And AccessManagementService.ОграничиватьДоступНаУровнеЗаписейУниверсально() Then
			
			// Планирование обновления доступа.
			СоставИзменений = Data.ChangeRowsContent.Copy(, "Table");
			СоставИзменений.GroupBy("Table");
			AccessManagementService.ЗапланироватьОбновлениеДоступаПриИзмененииТаблицГруппДоступа(СоставИзменений);
		EndIf;
		
		CommitTransaction();
	Except
		RollbackTransaction();
		Raise;
	EndTry;
	
EndProcedure
14. Common module ReportVariants
ReportDetails.Enable --> ReportDetails.Enabled
ReportDetails.Description = МетаданныеОтчета.Explanation; --> ReportDetails.LongDesc = МетаданныеОтчета.Explanation;
15. global replace Procedure ConfigureReportsVariants
Enable --> Enabled
ФункциональныеОпции, Описание, Размещение, ВидимостьПоУмолчанию - не перевелись
x. Common module PersonalDataProtection SEction.ID --> SEction.Identifier
17. Chart of characteristic types ChangingProhibitionDatesSections manager module
СвойстваРаздела.Reference --> СвойстваРаздела.Ref
18. Common module СПАРКРиски Procedure CONFIGUREReportsVariants
VariantSettings.Включен --> VariantSettings.Enabled
VariantSettings.Описание --> VariantSettings.LongDesc
19. Common module ReportsVariantsOverradable
.Описание --> .Definition
.Размещение --> .Placement
.Включен --> .Enabled
.Enable --> .Enabled
.ВидимостьПоУмолчанию --> .VisibleByDefault
.ФункциональныеОпции --> .FunctionalOptions
Variant.НастройкиДляПоиска --> Variant.SearchSettings

x. Reports StandardBOM Procedure ConfigureReportsVariants
.Описание --> .Definition
.НастройкиДляПоиска --> .SearchSettings
21. Common module ПроверкаКонтрагентовБРО Procedure CONFIGUREReportsVariants
.Enable --> .Enabled
22. Common module ReportsVariants Procedure UpdateKeysFixed
&Changes AS UPDATE --> &Changes AS Changes
x. Common module ReportsVariants Procedure MarkOnDeletionDeletedReportVariants
// Установка пометки удаления вариантов удаленных отчетов.
Procedure MarkOnDeletionDeletedReportVariants(Mode, Result)
	
	ProcedureRepresentation = StringFunctionsClientServer.PlaceParametersIntoString(
		NStr("ru = 'Удаление вариантов удаленных отчетов (%1)';
			|en = 'Удаление вариантов удаленных отчетов (%1)';"), 
		Lower(ModePresentation(Mode)));
	ЗаписатьВЖурналЗапускПроцедуры(ProcedureRepresentation);
	
	Query = New Query;
	QueryText =
	"SELECT
	|	ReportsVariants.Ref AS Ref
	|FROM
	|	Catalog.ReportsVariants AS ReportsVariants
	|WHERE
	|	NOT ReportsVariants.DeletionMark
	|	AND ReportsVariants.ReportType = &ReportType
	|	AND ISNULL(ReportsVariants.Report.DeletionMark, TRUE)";
	
	TableName = "Catalog.ReportsVariants";
	If Mode = "ОбщиеДанныеКонфигурации" Then
		QueryText = StrReplace(QueryText, ".ReportsVariants", ".PredefinedReportsVariants");
		QueryText = StrReplace(QueryText, "AND ReportsVariants.ReportType = &ReportType", "");
		TableName = "Catalog.PredefinedReportsVariants";
	ElsIf Mode = "ОбщиеДанныеРасширений" Then
		QueryText = StrReplace(QueryText, ".ReportsVariants", ".ПредопределенныеВариантыОтчетовРасширений");
		QueryText = StrReplace(QueryText, "AND ReportsVariants.ReportType = &ReportType", "");
		TableName = "Catalog.ПредопределенныеВариантыОтчетовРасширений";
	ElsIf Mode = "РазделенныеДанныеКонфигурации" Then
		Query.SetParameter("ReportType", Enums.ReportsTypes.Internal);
	ElsIf Mode = "РазделенныеДанныеРасширений" Then
		Query.SetParameter("ReportType", Enums.ReportsTypes.Extension);
	EndIf;
24. Common module ReportsVariants
УстановитьПометкуУдаления Возр --> SetDeletionMark ASC
VariantFromBase.Enable --> VariantFromBase.Enabled
Sort("Отчет, ПредопределенныйВариант, ИмяФункциональнойОпции") --> Sort("Report, PredefinedVariant, FunctionalOptionName")
	
x. Common module CommonUse 
&ПолноеИмяОбъектаМетаданных --> &FullMetadataObjectName
&ПутьКРеквизиту --> &PathToAttribute
x. Common module UsersService
|Служебный --> |SErvice
x. Common module UsersService
&ОтборПользователя --> &UserFilter
Пользователи.Ссылка В (&Пользователь) --> Users.Ref IN (&User)
&Отбор --> &Filter
&ПользовательИлиГруппа --> &UserOrGroup
СоставыГруппПользователей.Пользователь --> UsersGroupsContents.User
СоставыГруппПользователей.ГруппаПользователей --> UsersGroupsContents.UsersGroup
Пользователи.Ссылка = &Пользователь --> Users.Ref = &User
ВнешниеПользователи.Ссылка = &Пользователь --> ExternalUsers.Ref = &User
x. Information registers AccessValuesGroups Manager module 
&УсловиеОтбораПользователей1:СоставыГруппПользователей.Пользователь --> &UsersFilterCondition1:UsersGroupsContents.User
&УсловиеОтбораПользователей2:СтарыеДанные.ЗначениеДоступа --> &UsersFilterCondition2:OldData.AccessValue
&УсловиеОтбораГруппОбновляемыхДанных:СтарыеДанные.ГруппаДанных --> &UpdatedDataGroupsFilterCondition:OldData.DATAGroup
&УсловиеОтбораГруппПользователей1:СоставыГруппПользователей.ГруппаПользователей --> &UsersGroupsFilterCondition1:UsersGroupsContents.UsersGroup
&УсловиеОтбораГруппПользователей2:СтарыеДанные.ЗначениеДоступа --> &UsersGroupsFilterCondition2:OldData.AccessValue
x. Common module AccessManagementService
ТИПЗНАЧЕНИЯ(ПроверяемыеПользователи.Пользователь) = ТИП(Справочник.Пользователи) --> VALUETYPE(CheckedUsers.User) = TYPE(Catalog.Users)
x. common module InfobaseUpdateService
SubsystemDescriptions.Order --> SubsystemDescriptions.SortOrder
ОбщиеДанные Убыв --> SharedData Desc
StandardSubsystemsReUse.SubsystemDescriptions().Order --> StandardSubsystemsReUse.SubsystemDescriptions().SortOrder
x. Global replace
Handler.ID  --> Handler.Identifier 
Priority.Order = --> Priority.SortOrder =
ParametersKind.Order --> ParametersKind.SortOrder
.Order = "Before" --> .SortOrder = "Before"
.Order = "Any" --> .SortOrder = "Any"
.Order = "After" --> .SortOrder = "After"
Setting.Key --> Setting.KeyVal
PrintCommand.ID --> PrintCommand.Identifier 
PrintCommand.Order --> PrintCommand.SortOrder 
Setting.ID --> Setting.Identifier 
Set.ID --> Set.Identifier 
Descriptor.ID --> Descriptor.Identifier 
ОписаниеКлассификатора.ID --> ОписаниеКлассификатора.Identifier 
"Обработки. --> "DataProcessors.
Очередь Возр --> Queue Asc
31. Common module ЦенообразованиеСервер Procedure СнятьПризнакНовинка()
Query.Text	= "SELECT
	|	CatProductsAndServices.Ref
	|FROM
	|	Catalog.ProductsAndServices КАК CatProductsAndServices
	|Where
	|	CatProductsAndServices.ЭтоНовинка
	|	AND CatProductsAndServices.СрокДействияФлагаНовинка <= &СрокДействияФлагаНовинка
	|	AND CatProductsAndServices.СрокДействияФлагаНовинка <> DATETIME(01, 01, 01, 00, 00, 00)";
32. Common modules ScheduledJobsService
РегламентноеЗадание --> ScheduledJob
33. Common module DataExchangeXDTOServer
Версия Убыв --> Version DESC
x. Catalog AcessGroupsProfiles manager module
ProfileProperties.ID --> ProfileProperties.Identifier
x. Common modules РаботаСКлассификаторами
RegClassifierVersions.ID --> RegClassifierVersions.Identifier
x. Common module StandardSubsystemsIntegration
"Обработки. --> "DataProcessors.
ЗагрузкаКурсовВалют --> CurrencyRatesImportProcessCurrencyRatesImportProcess
37. Day kinds translation in DataProcessors.ЗаполнениеКалендарныхГрафиков.BusinessCalendarsData
Рабочий = Work, а по словарю должен быть Working!
38. Common module UsersService Procedure UpdateUsersPredefinedContactInformationTypes()
ParametersKind.Type --> ParametersKind.Kind
39. Common module РаботаСКлассификаторами
КэшДанныхКлассификаторов.ДанныеФайла КАК ДанныеФайла, --> КэшДанныхКлассификаторов.FILEData КАК FILEData,
ВерсииКлассификаторов.Наименование КАК Наименование --> ВерсииКлассификаторов.Description КАК Description
40. Common module CommonUse
БезопасноеХранилищеДанных --> SafeDataStorage
Владелец --> Owner
41. Catalog WorldCountries.Clussifier - template column names translation
42. Information registers AddressObjects.RFTerritorialEntitiesClassifier - template column names translation
43. Catalog.ИдентификаторыСервисовСопровождения.ManagerModule
ОписательСервиса.ID --> ОписательСервиса.Identifier
44. Information registers ДоступныеВерсииАгентаКопированияОблачногоАрхива.СтандартныеЗначения - template column names translation
45. Chart of characteristic types КатегорииНовостей.СтандартныеЗначения - template column names translation
46. Catalog.ЛентыНовостей.СтандартныеЗначения - template column names translation
47. Settings storages НастройкиНовостей manager module
Procedure ImportProcessing --> LoadProcessing
48. Common module EquipmentManagerServerCall
ПолноеИмяТаблицы --> FullTableName
49. Common module ObjectPrefixationEvents Procedure SetPrefix
	PrefixTemplate = "[OR][IB]-[Prefix]";
	PrefixTemplate = StrReplace(PrefixTemplate, "[OR]", CompanyPrefix); // [ОР]
	PrefixTemplate = StrReplace(PrefixTemplate, "[IB]", InfobasePrefix); // [Infobase]
	PrefixTemplate = StrReplace(PrefixTemplate, "[Prefix]", Prefix);
50. Catalog.PriceKinds.ManagerModule.ПодготовитьТаблицуРегистраОчередьРасчетаЦен
Procedure ПодготовитьТаблицуРегистраОчередьРасчетаЦен(ОчередьРасчетаЦен, СвязиВидовЦен, ThisObject)
	
	Query 			= New Query;
	Query.Text	= 
	"SELECT различные &ВидЦенРасчетный AS ВидЦенРасчетный, ЗаписиЦен.ProductsAndServices, ЗаписиЦен.Characteristic, False AS ПересчетВыполнен, &Period AS RecordPeriod
	|from InformationRegister.ProductsAndServicesPrices.SliceLast(,Actuality AND PriceKind IN(&PriceKindsArray)) AS ЗаписиЦен
	|Union
	|SELECT различные &ВидЦенРасчетный AS ВидЦенРасчетный, ЗаписиЦенКонтрагентов.ProductsAndServices, ЗаписиЦенКонтрагентов.Characteristic, False AS ПересчетВыполнен, &Period AS RecordPeriod
	|from InformationRegister.CounterpartyProductsAndServicesPrices.SliceLast(,Actuality AND CounterpartyPriceKind IN(&PriceKindsArray)) AS ЗаписиЦенКонтрагентов
	|Упорядочить по ВидЦенРасчетный, ProductsAndServices, Characteristic";
	
	Query.SetParameter("Period", CurrentSessionDate());
	Query.SetParameter("ВидЦенРасчетный", ThisObject.Ref);
	Query.SetParameter("PriceKindsArray", СвязиВидовЦен.UnloadColumn("ВидЦенБазовый"));
	
	ОчередьРасчетаЦен = Query.Execute().Unload();
	
EndProcedure
51. common module SmallBusinessServer
Procedure ПодготовитьТаблицуРегистраОчередьРасчетаЦен(ОчередьРасчетаЦен, СвязиВидовЦен, ThisObject)
	
	Query 			= New Query;
	Query.Text	= 
	"SELECT различные &ВидЦенРасчетный AS ВидЦенРасчетный, ЗаписиЦен.ProductsAndServices, ЗаписиЦен.Characteristic, False AS ПересчетВыполнен, &Period AS RecordPeriod
	|from InformationRegister.ProductsAndServicesPrices.SliceLast(,Actuality AND PriceKind IN(&PriceKindsArray)) AS ЗаписиЦен
	|Union
	|SELECT различные &ВидЦенРасчетный AS ВидЦенРасчетный, ЗаписиЦенКонтрагентов.ProductsAndServices, ЗаписиЦенКонтрагентов.Characteristic, False AS ПересчетВыполнен, &Period AS RecordPeriod
	|from InformationRegister.CounterpartyProductsAndServicesPrices.SliceLast(,Actuality AND CounterpartyPriceKind IN(&PriceKindsArray)) AS ЗаписиЦенКонтрагентов
	|Упорядочить по ВидЦенРасчетный, ProductsAndServices, Characteristic";
	
	Query.SetParameter("Period", CurrentSessionDate());
	Query.SetParameter("ВидЦенРасчетный", ThisObject.Ref);
	Query.SetParameter("PriceKindsArray", СвязиВидовЦен.UnloadColumn("ВидЦенБазовый"));
	
	ОчередьРасчетаЦен = Query.Execute().Unload();
	
EndProcedure
52. InformationRegister.AccessGroupsValues.ManagerModule.RefreshDataRegister
AccessTypeProperties.Reference --> AccessTypeProperties.Ref
53. common module InfobaseUpdateSB
Procedure ОбновитьПредопределенныеВидыЦен() Export
	
	// Наличие видов цен контрагентов в новой ИБ не предполагается, но такую возможность поддержим...
	
	Query = New Query(
	"Select Catalog.PriceKinds.Ref AS PriceKind, True AS ЭтоЦеныНоменклатуры 
	|	Where Catalog.PriceKinds.ИдентификаторФормул = """" OR Catalog.PriceKinds.ТипВидаЦен = VALUE(Enum.ТипыВидовЦен.EMPTYRef)
	|
	|Union All
	|
	|Select Catalog.CounterpartyPRICEKind.Ref, False Where Catalog.CounterpartyPRICEKind.ИдентификаторФормул = """"");
	
	QueryResult = Query.Execute();
54. common module ЦенообразованиеФормулыСервер
Function НайтиВидЦенПоИдентификатору(Id, ExcludingRef = Undefined) Export
	
	Result = New Structure("ИдентификаторЗанят, PriceKind, ЭтоЦеныНоменклатуры", False, Undefined, Undefined);
	
	Query = New Query(
	"select Catalog.PriceKinds.Ref AS PriceKind, True AS ЭтоЦеныНоменклатуры Where Catalog.PriceKinds.ИдентификаторФормул = &Id
	|Union All 
	|select Catalog.CounterpartyPRICEKind.Ref, False Where Catalog.CounterpartyPRICEKind.ИдентификаторФормул = &Id");
	
	Query.SetParameter("Id", Id);
	Selection = Query.Execute().Select();
55. Catalog.PriceKinds.ManagerModule.ОчиститьСвязиВидовЦен(PriceKind = Оптовая цена) line: 226	
Procedure ОчиститьСвязиВидовЦен(PriceKind) Export
	
	Query = New Query("select top 1 СвязиВидовЦен.ВидЦенРасчетный from InformationRegister.СвязиВидовЦенСлужебный AS СвязиВидовЦен Where СвязиВидовЦен.ВидЦенРасчетный = &ВидЦенРасчетный");
	Query.SetParameter("ВидЦенРасчетный", PriceKind);
	
	Selection = Query.Execute().Select();
56. Common module DataExchangeServer - lot of changes!
57. Common module НастройкаПрограммыПереопределяемый and DataProcessor.НастройкаПрограммы.ManagerModule global replace
НоваяНастройка.СвойстваЭлемента.Вставить("Вид", ...
здесь Вид нужно перевести как Type!
НоваяНастройка.ItemProperties.Insert("Type", ... 




		